import numpy as np

def dft_grid(N):
    """
    Generates an N-point DFT grid on the interval [0, 2*pi].
    """

    return 2*np.pi/N * np.linspace(0, N-1, N)

def ns_from_N(N):
    """
    Returns integer frequencies associated to an N-point DFT.
    """

    if N % 2 == 1:
        return np.linspace(-(N-1)/2., (N-1)/2, N)
    else:
        return np.linspace(-N/2, (N-2)/2, N)

def fft(u):
    """
    Returns the DFT of a vector u, computed using numpy's FFT package.
    """

    return np.fft.fftshift(np.fft.fft(u))/len(u)

def ifft(U):
    """
    Returns the inverse DFT of a vector U, computed using numpy's FFT package.
    """

    return np.fft.ifft(np.fft.ifftshift(U))*len(U)

if __name__ == "__main__":

    from numpy import sin, cos
    N = 13

    f = lambda x: sin(3*x)

    x = dft_grid(N)
    ns = ns_from_N(N)

    F = fft(f(x))
    fi = ifft(F)
