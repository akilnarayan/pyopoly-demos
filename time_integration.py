import numpy as np

def forward_euler_step(u, t, ode_rhs, k):
    """
    Performs one forward-Euler update for the ODE

        u'(t) = ode_rhs(u, t)

    from the current state u, with a stepsize h.
    """

    assert k > 0

    return u + k*ode_rhs(u, t)

def rk4_step(u, t, ode_rhs, k):
    """
    Performs one update of the "standard" 4-stage, 4th order RK method
    applied to the ODE

        u'(t) = ode_rhs(u, t)

    from the current state u, with a stepsize h.
    """

    assert k > 0
    k1 = k*ode_rhs(u,t)
    k2 = k*ode_rhs(u+k1/2., t + k/2.)
    k3 = k*ode_rhs(u+k2/2., t + k/2.)
    k4 = k*ode_rhs(u+k3, t + k)

    return u + 1./6.*(k1 + 2*k2 + 2*k3 + k4)
