# Demonstration of constructing interpolants on Gauss quadrature nodes

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rcParams
from pyopoly.families import JacobiPolynomials

# Font styling
rcParams['font.family'] = 'serif'
rcParams['font.weight'] = 'bold'
fontprops = {'fontweight': 'bold'}

# Test function
u = lambda xx: np.cos(50*xx)*np.exp(-10*(xx-1/np.pi)**2)

N = np.arange(5, 100,dtype=int)
Nplots = [25, 50, 75]

# Jacobi polynomials
alpha = 0.
beta = 0.
J = JacobiPolynomials(alpha=alpha, beta=beta)

# Grid for computing errors
M = 2000
xg, wg = J.gauss_quadrature(M)

# Error storage allocation
l2_errors = np.zeros(N.size)
linf_errors = np.zeros(N.size)
Verr = J.eval(xg, range(np.max(N)))
uapprox = []

for (ind_n, n) in enumerate(N):

    # Compute interpolation grid
    x,w = J.gauss_quadrature(n)

    # Compute interpolant
    c = np.dot(J.eval(x, range(n)).T, w*u(x))

    interp_eval = np.dot(Verr[:,:n], c)
    l2_errors[ind_n] = np.sqrt(np.sum(wg*(interp_eval - u(xg))**2))
    linf_errors[ind_n] = np.max(interp_eval - u(xg))

    if n in Nplots:
        uapprox.append(interp_eval)

plt.subplot(1,2,1)
plt.semilogy(N, l2_errors)
plt.xlabel(r'$N$', **fontprops)
plt.title(r'$L^2_w$ error of $N$-point interpolant', **fontprops)

plt.subplot(1,2,2)
plt.semilogy(N, linf_errors)
plt.xlabel(r'$N$', **fontprops)
plt.title(r'$L^\infty$ error of $N$-point interpolant', **fontprops)

plt.figure()
lines = []
lines.append(plt.plot(xg, u(xg))[0])
for uvals in uapprox:
    lines.append(plt.plot(xg, uvals)[0])

plt.xlabel(r'$x$', **fontprops)
plt.title(r'Plot of $u$ and $N$-point interpolants', **fontprops)

labels = [r'Function $u(x)$',]
for n in Nplots:
    labels.append(r'Interpolant, $N = {0:d}$'.format(n))

plt.legend(lines, labels)
plt.show()
