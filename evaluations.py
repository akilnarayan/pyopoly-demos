# Demonstration of evaluation of orthogonal polynomials and derivatives

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rcParams
from pyopoly.families import JacobiPolynomials, HermitePolynomials

# Number of polynomials to plot
N = 10
M = 501

# Jacobi polynomials
alpha = 0.
beta = 0.
J = JacobiPolynomials(alpha=alpha, beta=beta)
x = np.linspace(-1, 1, M)
# Create Vandermonde-like matrix
V = J.eval(x, range(N))

# Derivatives of Jacobi polynomials
d = 2
Vd = J.eval(x, range(d,N), d)

# Font styling
rcParams['font.family'] = 'serif'
rcParams['font.weight'] = 'bold'
fontprops = {'fontweight': 'bold'}

plt.plot(x, V)
plt.xlabel(r'$x$', **fontprops)
plt.ylabel(r'$p_n$', **fontprops)
title_string = r'Jacobi polynomials ($\alpha = {0:.2f}$, $\beta={1:.2f})$'.format(alpha, beta)
plt.title(title_string, **fontprops)

plt.figure()
plt.plot(x, Vd)
plt.ylabel(r'$p^{{({0:d})}}_n$'.format(d), **fontprops)
plt.xlabel(r'$x$')
title_string = r'Jacobi polynomial derivatives ($\alpha = {0:.2f}$, $\beta={1:.2f})$'.format(alpha, beta)
plt.title(title_string, **fontprops)

# Hermite polynomials
H = HermitePolynomials()
xh = np.linspace(-np.sqrt(2*N), np.sqrt(2*N), M)
Vh = H.eval(xh, range(N))

plt.figure()
plt.plot(xh, Vh)
plt.xlabel(r'$x$', **fontprops)
plt.ylabel(r'$p_n$', **fontprops)
title_string = r'Hermite polynomials'
plt.title(title_string, **fontprops)

plt.figure()
plt.plot(xh, (np.sqrt(np.exp(-xh**2))*Vh.T).T)
plt.xlabel(r'$x$', **fontprops)
plt.ylabel(r'$\sqrt{w(x)} p_n(x)$', **fontprops)
title_string = r'Weighted Hermite polynomials'
plt.title(title_string, **fontprops)

plt.show()


#############

p = lambda xx: some_function(xx)

new_integrand = lambda(xx): p((xx+1.)/2.) + p((xx-1.)/2.)

J = some_definition_here

def old_integrand(xx,nn):
    return J.eval(xx, nn)*f(x)

def new_integrand2(xx,nn):
    return old_integrand((xx+1.)/2.,nn) + old_integrand((xx-1.)/2.,nn)


new_w = old_w/2.

np.sum(new_w * new_integrand2(old_x,nn))


