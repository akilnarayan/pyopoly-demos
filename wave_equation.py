import numpy as np
from matplotlib import pyplot as plt
import matplotlib.animation as animation

from fourier_utils import dft_grid, ns_from_N, fft, ifft
from time_integration import rk4_step, forward_euler_step

animate = True

# Define PDE:
#
#  u_t = a(x) u_x,
#  u(x,0) = u0(x)
#  + periodic boundary conditions
u0 = lambda x: np.exp(np.sin(3*x))
a = lambda x: np.ones(x.shape)

# Terminal time
T = 5
# Number of spatial grid points
N = 100
# CFL condition
dt = 1/(N*np.max(np.abs(a(np.linspace(0, 2*np.pi, 1000)))))

t = 0.

x = dft_grid(N)
u = np.ndarray.astype(u0(x), 'complex128')

# Modal differentiation matrix is diagonal:
Dvec = 1j*ns_from_N(N)
ax = a(x)

if animate:
    Nsteps = int(np.ceil(T/dt))
    U = np.zeros((N, 1 + Nsteps), dtype='complex128')
    U[:,0] = u
    step = 0

def ode_rhs(uu,tt):
    return ax*ifft(Dvec*fft(uu))

while t < T - 1e-12:

    k = np.min((dt, T-t))

    # Forward Euler
    #u = forward_euler_step(u, t, ode_rhs, k)

    # RK4
    u = rk4_step(u, t, ode_rhs, k)

    t += k

    if animate:
        U[:,step+1] = u
        step += 1


if animate:
    fig = plt.figure()
    plt.plot(x, np.real(U[:,0]), 'k:')
    line = plt.plot([], [], 'r')[0]
    ax = plt.gca()
    time_template = 'time = %.3fs'
    time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)

    def animation_init():
        time_text.set_text('')
        return line, time_text

    def animation_update(i):
        line.set_data(x, np.real(U[:,i]))
        time_text.set_text(time_template % (i*dt))
        return line, time_text

    ani = animation.FuncAnimation(fig, animation_update, np.arange(0, Nsteps, 1),
                      interval=1, blit=True, init_func=animation_init, repeat_delay=3000)

    plt.show()
