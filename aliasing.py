# Aliasing error demonstrations

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rcParams
from pyopoly.families import JacobiPolynomials

rcParams['font.family'] = 'serif'
rcParams['font.weight'] = 'bold'
fontprops = {'fontweight': 'bold'}

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

# Illustrate aliasing of p_n using N-point interpolation on Gaussian
# quadrature nodes, for N << n.
n = 40
N = 11

test1 = {'alpha':-0.5, 'beta':-0.5, 'n':n, 'N':N}
test2 = {'alpha':np.pi, 'beta':10., 'n':n, 'N':N}

# First test
J = JacobiPolynomials(**{key:test1[key] for key in ['alpha', 'beta']})

# Grids for interpolation, plotting
x, w = J.gauss_quadrature(N)
xg, wg = J.gauss_quadrature(1000)

# Compute p_n interpolant
pnx = J.eval(x, n)
c = np.dot(J.eval(x, range(N)).T, w*pnx[:,0])
pn_aliased = np.dot(J.eval(xg, range(N)), c)
pnxg = J.eval(xg, n)[:,0]

plt.subplot(1,2,1)
lines = []
lines.append(plt.plot(xg, pnxg, 'b')[0])
lines.append(plt.plot(x, pnx, 'r.', markersize=10)[0])
lines.append(plt.plot(xg, pn_aliased, 'r:')[0])
plt.xlabel(r'$x$', **fontprops)
plt.title(r'Chebyshev polynomials (n,N) = ({0:d},{1:d}), $\| p_n\| = 1$, $\| I_N \Pi_N^\perp p_n\| = {2:1.3f}$'.format(n,N,np.linalg.norm(c)), **fontprops)
plt.xlim((-1,1))
plt.ylim((-2,1.5))
plt.legend(lines, [r'$p_n$', r'Interpolation points', r'$p_n$ aliased'])

# Second test
J = JacobiPolynomials(**{key:test2[key] for key in ['alpha', 'beta']})

# Grid for interpolation, plotting
x, w = J.gauss_quadrature(N)
xg, wg = J.gauss_quadrature(1000)

# Compute p_n interpolant
pnx = J.eval(x, n)
c = np.dot(J.eval(x, range(N)).T, w*pnx[:,0])
pn_aliased = np.dot(J.eval(xg, range(N)), c)
pnxg = J.eval(xg, n)[:,0]

plt.subplot(1,2,2)
lines = []
lines.append(plt.plot(xg, pnxg, 'b')[0])
lines.append(plt.plot(x, pnx, 'r.', markersize=10)[0])
lines.append(plt.plot(xg, pn_aliased, 'r:')[0])
plt.xlabel(r'$x$', **fontprops)
plt.title(r'Jacobi polynomials, $(\alpha,\beta) = ({3:1.3f},{4:1.3f})$, (n,N) = ({0:d},{1:d}), $\| p_n\| = 1$, $\| I_N \Pi_N^\perp p_n\| = {2:1.3f}$'.format(n,N,np.linalg.norm(c),test2['alpha'], test2['beta']), **fontprops)
plt.ylim((-30,35))
plt.legend(lines, [r'$p_n$', r'Interpolation points', r'$p_n$ aliased'])

## Now compute aliasing errors for many n/N values
ns = range(1, 101)
Ns = range(1, 101)

J1 = JacobiPolynomials(**{key:test1[key] for key in ['alpha', 'beta']})
J2 = JacobiPolynomials(**{key:test2[key] for key in ['alpha', 'beta']})

aliasing1 = np.zeros((len(Ns), len(ns)))
aliasing2 = np.zeros((len(Ns), len(ns)))

# For each (N,n), compute N-point interpolant aliasing error for p_n
for (Nind,N) in enumerate(Ns):
    x1, w1 = J1.gauss_quadrature(N)
    V1T = J1.eval(x1, range(N)).T
    x2, w2 = J2.gauss_quadrature(N)
    V2T = J2.eval(x2, range(N)).T

    for (nind,n) in enumerate(ns):

        if n <= N:
            pass
        else:
            aliasing1[Nind, nind] = np.linalg.norm( np.dot(V1T, w1*J1.eval(x1, n)[:,0] ) )
            aliasing2[Nind, nind] = np.linalg.norm( np.dot(V2T, w2*J2.eval(x2, n)[:,0] ) )

plt.figure()
plt.subplot(1,2,1)
xvals = np.concatenate((np.asarray(Ns)-0.5, [Ns[-1]+0.5,]))
yvals = np.concatenate((np.asarray(ns)-0.5, [ns[-1]+0.5,]))
plt.pcolor(xvals, yvals, aliasing1.T, cmap='inferno')
plt.colorbar()
plt.xlabel(r'$N$', **fontprops)
plt.ylabel(r'$n$', **fontprops)
plt.title(r'Chebyshev polynomials, $\| I_N \Pi_N^\perp p_n\|$', **fontprops)

plt.subplot(1,2,2)
xvals = np.concatenate((np.asarray(Ns)-0.5, [Ns[-1]+0.5,]))
yvals = np.concatenate((np.asarray(ns)-0.5, [ns[-1]+0.5,]))
plt.pcolor(xvals, yvals, aliasing2.T, cmap='inferno')
plt.colorbar()
plt.xlabel(r'$N$', **fontprops)
plt.ylabel(r'$n$', **fontprops)
plt.title(r'Jacobi polynomials $(\alpha,\beta) = ({0:1.3f},{1:1.3f})$, $\| I_N \Pi_N^\perp p_n\|$'.format(test2['alpha'], test2['beta']), **fontprops)

plt.show()
